//
//  main.swift
//  SudokuCompact
//
//  Created by Eddie Draaisma on 05/02/2019.
//  Copyright © 2019 Eddie Draaisma. All rights reserved.
//

import Foundation


func rowFromRCV(_ r: Int, _ c: Int, _ v: Int) -> Int {
    return ((r * 9) + c) * 9 + (v - 1)
}


func rowToRCV(_ val: Int) -> (Int, Int, Int) {
    let v = (val % 9) + 1
    let c = (val / 9) % 9
    let r = (val / 81) % 9
    return(r, c, v)
}


func constraintsForRow(_ row: Int) -> [Int] {
    let (r, c, v) = rowToRCV(row)
    let b = (r / 3) * 3 + c / 3
    
    return [9 * r + c, 81 + 9 * r + v - 1, 162 + 9 * c + v - 1, 243 + 9 * b + v - 1]
}


func makeRowConstraints() -> [[Int]] {
    var X = Array(repeating: [-1], count: 4 * 9 * 9)
    
    for row in 0 ..< (9 * 9 * 9) {
        for constraint in constraintsForRow(row) {
            X[constraint].append(row)
        }
    }
    return X
}



func doSelectRow(_ X: inout [[Int]], _ row: Int) -> [[Int]] {
    
    var cols: [[Int]] = []
    
    for constraint1 in constraintsForRow(row) {
        for cell in X[constraint1] {
            for constraint2 in constraintsForRow(cell) {
                if constraint2 != constraint1 {
                    X[constraint2] = X[constraint2].filter {$0 != cell }
                }
            }
        }
        cols.append(X[constraint1])
        X[constraint1] =  []
    }
    return cols
}


func undoSelectRow(_ X: inout [[Int]], _ row: Int, _ cols: inout [[Int]]) {
    for constraint1 in constraintsForRow(row).reversed() {
        X[constraint1] = cols.removeLast()
        for cell in X[constraint1] {
            for constraint2 in constraintsForRow(cell) {
                if constraint2 != constraint1 {
                    X[constraint2].append(cell)
                }
            }
        }
    }
}


func solveEC(_ X: inout [[Int]]) -> [Int] {
    var solution = Array<Int>()
    
    func solveLocal(_ X: inout [[Int]]) -> [Int]? {
        
        var minPos = X.count
        var minValue = Int.max
        for pos in 0 ..< X.count {
            if (X[pos].count > 0) && (X[pos].count < minValue)  {
                minPos = pos
                minValue = X[pos].count
            }
        }
        if minValue == Int.max {
            return solution
        }
        for row in X[minPos][1...] {
            solution.append(row)
            var cols = doSelectRow(&X, row)
            if let s = solveLocal(&X) {
                return s
            }
            undoSelectRow(&X, row, &cols)
            _ = solution.popLast()
        }
        return nil
    }
    
    
    return solveLocal(&X) ?? []
}


func boardSolver(_ board: [Int]) -> [Int] {
    var board = board
    var X = makeRowConstraints()
    
    for pos in 0 ..< 81 {
        let v = board[pos]
        if v != 0 {
            let _ = doSelectRow(&X, rowFromRCV(pos / 9, pos % 9, v))
        }
    }
    
    let solution = solveEC(&X)
    
    for row in solution {
        let (r, c, v) = rowToRCV(row)
        board[9 * r + c] = v
    }
    return board
}


func makeBoard(_ gameStr: String) -> [Int] {
    
    var board = Array(repeating: 0, count: 81)
    var pos = 0
    for chr in gameStr {
        board[pos] = Int(String(chr)) ?? 0
        pos += 1
    }
    return board
}


func printBoard(_ board: [Int]) {
    print()
    for row in 0 ..< 9 {
        if [3, 6].contains(row) {
            print(" ------+-------+------")
        }
        print(" ", terminator: "")
        for col in 0 ..< 9 {
            if [3, 6].contains(col) {
                print("| ", terminator: "")
            }
            print(board[row * 9 + col], terminator: " ")
        }
        print()
    }
    print()
}


let grid1 = "003020600900305001001806400008102900700000008006708200002609500800203009005010300"
let hard1 = ".....6....59.....82....8....45........3........6..3.54...325..6.................."
let most  = "061007003092003000000000000008530000000000504500008000040000001000160800600000000"
let grid2 = "4.....8.5.3..........7......2.....6.....8.4......1.......6.3.7.5..2.....1.4......"
let difficult = "800000000003600000070090200050007000000045700000100030001000068008500010090000400"

let diff = "600008940900006100070040000200610000000000200089002000000060005000000030800001600"
let diff2 = "980700000600090800005008090500006080000400300000070002090300700050000010001020004"

let xx = "...8.1..........435............7.8........1...2..3....6......75..34........2..6.."

let snyder = " 5  2  3 2    17 84 76          5   52     47   7          35 43 65    1 9  7  6 "
let empty = "1234............................................................................."


let gameStr = diff


printTimeElapsedWhenRunningCode {
    printBoard(boardSolver(makeBoard(gameStr)))
}

