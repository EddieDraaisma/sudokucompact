//
//  Timer.swift
//  SudokuCompact
//
//  Created by Eddie Draaisma on 05/02/2019.
//  Copyright © 2019 Eddie Draaisma. All rights reserved.
//

import Foundation

func printTimeElapsedWhenRunningCode(_ operation:() -> ()) {
    let startTime = CFAbsoluteTimeGetCurrent()
    operation()
    let timeElapsed = CFAbsoluteTimeGetCurrent() - startTime
    print("Time elapsed : \(timeElapsed)s")
}

