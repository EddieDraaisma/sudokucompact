//
//  OldMethod.swift
//  SudokuCompact
//
//  Created by Eddie Draaisma on 06/02/2019.
//  Copyright © 2019 Eddie Draaisma. All rights reserved.
//

import Foundation



struct colConstraint: Hashable {
    
    let text: String
    let value1: Int
    let value2: Int
    
    init(_ t: String, _ v1: Int, _ v2: Int) { text = t; value1 = v1; value2 = v2 }
}


struct rowSolution: Hashable, Equatable {
    
    let row: Int
    let col: Int
    let value: Int
    init(_ r: Int, _ c: Int, _ v: Int) { row = r; col = c; value = v }
}


func rowConstraints(row: rowSolution) -> Array<colConstraint> {
    
    let r = row.row
    let c = row.col
    let v = row.value
    
    let b = (r / 3) * 3 + c / 3
    return [colConstraint("rc", r, c),
            colConstraint("rn", r, v),
            colConstraint("cn", c, v),
            colConstraint("bn", b, v)]
}



func select(_ X: inout [colConstraint: Set<rowSolution>], _ row: rowSolution) -> [Set<rowSolution>] {
    
    var cols: [Set<rowSolution>] = []
    
    for constraint1 in rowConstraints(row: row){
        if X[constraint1]!.count == 0 {
            
        }
        for cell in X[constraint1]! {
            for constraint2 in rowConstraints(row: cell) {
                if constraint1 != constraint2 {
                    X[constraint2]!.remove(cell)
                }
            }
        }
        cols.append(X.removeValue(forKey: constraint1)!)
    }
    return cols
}



func deselect(_ X: inout [colConstraint: Set<rowSolution>], _ row: rowSolution, _ cols: inout [Set<rowSolution>]) {
    for constraint1 in rowConstraints(row: row).reversed() {
        X[constraint1] = cols.popLast()
        for cell in X[constraint1]! {
            for constraint2 in rowConstraints(row: cell) {
                if constraint2 != constraint1 { X[constraint2]!.insert(cell) }
            }
        }
    }
}


var counter = 0

func solve(_ X: inout [colConstraint: Set<rowSolution>]) -> [rowSolution] {
    
    var solution: [rowSolution] = []
    
    func solveLocal(_ X: inout [colConstraint: Set<rowSolution>]) -> [rowSolution]? {
        counter += 1
        if X.isEmpty {
            return solution
        }
        else {
            let c = X.min(by: {$0.value.count < $1.value.count})!
            for row in c.value {
                solution.append(row)
                var cols = select(&X, row)
                if let s = solveLocal(&X) {
                    return s
                }
                deselect(&X, row, &cols)
                _ = solution.popLast()
            }
        }
        return nil
    }
    return solveLocal(&X) ?? []
}



func solveBoard(_ board: [Int]) -> [Int] {
    var board = board
    var X: [colConstraint: Set<rowSolution>] = [:]
    
    for row in 0 ..< 9 {
        for col in 0 ..< 9 {
            for num in 1 ... 9 {
                let celConstraints = rowConstraints(row: rowSolution(row, col, num))
                for constraint in celConstraints {
                    X[constraint, default: []].insert(rowSolution(row, col, num))
                }
            }
        }
    }
    for pos in 0 ..< 81 {
        if board[pos] != 0 {
            let _ = select(&X, rowSolution(pos / 9, pos % 9, board[pos]))
        }
    }
    let solution = solve(&X)
    for row in solution {
        board[row.row * 9 + row.col] = row.value
    }
    return board
}

